

import path from "path"
import fs from "fs"

import Aura from 'pg-aura'

import readline from 'readline'
import { Writable } from 'stream'

import crypto from "crypto"

export default class {
  static async prepare() {
    try {
      let mutableStdout = new Writable({
        write: function(chunk, encoding, callback) {
          if (!this.muted)
            process.stdout.write(chunk, encoding);
          callback();
        }
      });

      let rl = readline.createInterface({
        input: process.stdin,
        output: mutableStdout,
        terminal: true
      });

      rl.questionSync = async function(msg, pwd) {
        return await new Promise(function(resolve) {
          mutableStdout.muted = false;
          rl.question(msg, function(input) {
            resolve(input);
          });
          if (pwd) mutableStdout.muted = true;
        });
      };

      console.log("");

      const env_path = path.resolve(process.cwd(), "env.json");
      let env = {};
      if (fs.existsSync(env_path)) {
        console.log("There is already an `env.json` file existing in your current working directory.");

        try {
          env = JSON.parse(fs.readFileSync(env_path, 'utf8'));
        } catch (e) {
          console.error(e.stack);
          process.exit();
        }

        console.log("\x1b[31mThe `db_pwd` option is already specified in this environment!", "\x1b[0m");
        let proceed = await rl.questionSync('Continue[yN]: ');
        if (!proceed || proceed.toLowerCase() !== "y") process.exit();
      }


      env.name = await rl.questionSync('App name: ');

      console.log("");
      console.log("\x1b[36m>>> Authenticate with you PostgresSQL admin account <<<\x1b[0m");

      let db_user = await rl.questionSync('User[postgres]: ');
      if (!db_user) db_user = "postgres";

      const db_pwd = await rl.questionSync('Password: ', true);
      rl.close();

      env.db_user = env.db_name = env.name.replace(/\s+/g, '').replace('.', '_').replace('-', '_').toLowerCase();
      env.db_pwd = crypto.randomBytes(10).toString('hex');

      let super_aura = await Aura.Super.connect(db_user, db_pwd);
      await super_aura.create_database(env.db_name, db_user, db_pwd);
      await super_aura.create_user(env.db_name, env.db_pwd);
      await super_aura.grant_privileges(env.db_name, env.db_name);
      super_aura.disconnect();

      console.log("");
      console.log("Database and user`"+env.db_name+"` have been created.");

      fs.writeFileSync(env_path, JSON.stringify(env, null, 2), 'utf8');
      console.log("Configuration saved to:", env_path);
    } catch (e) {
      console.error(e.stack);
    }
  }
}

#!/usr/bin/env node
import fs from 'fs-extra'
import path from 'path'



let command = process.argv[2];

import rquest from 'request'
let request = rquest.defaults({jar: true});

import http from 'https'
//const http = require('http');


//const db_addr = 'https://mellisuga.org/';
const db_addr = 'http://127.0.0.1:9646/';
//const db_addr = 'https://mellisuga.subtledev.space/';
//const db_addr = 'http://192.168.0.124:9639/';


let cache_dir = path.resolve(process.cwd(), ".mellisuga");

import compressing from 'compressing'

const db_host = "mellisuga.subtledev.space", db_port = 443;
//const db_host = "127.0.0.1", db_port = 9646;


import XHR from "./util/xhr_async.js"



import FlyAuth from "./util/flyauth/index.js"

import PM from "./pm.js"

import ENV from "./env/index.js"

import CLIScript from "./script/index.js"


(async function() {
  try {



/*
        let mresp = await XHR.post(db_host, db_port, "/modules.io", {
          command: "publish",
          module: {}
        });
        console.log(mresp);
*/
    await PM.create_directories();


    let flyauth = undefined;
    switch(command) {
      case 'new':
        switch(process.argv[3]) {// WHAT
          case 'app':
            await PM.init();
            break;
          case 'page':
            await PM.new_page();
            break;
          default:
            console.log("Error: unknown type `"+process.argv[3]+"`!");
        }
        break;
      case 'env':
        switch(process.argv[3]) {// WHAT
          case 'create':
            await ENV.prepare();
            break;
          default:
            console.log("Error: unknown env function `"+process.argv[3]+"`!");
        }
        break;
      case 'script':
        await CLIScript.run();
        break;
      case 'signin':
        flyauth = await FlyAuth.construct(db_host, db_port)
        await flyauth.user_session(true)
        break;
      case 'signout':
        flyauth = await FlyAuth.construct(db_host, db_port)
        await flyauth.terminate_session()
        break;
      case 'publish':
        flyauth = await FlyAuth.construct(db_host, db_port);
        if (!await flyauth.user_session()) {
          console.log("");
          console.log("\x1b[31mAUTHENTICATION FAILED!\x1b[0m");
          console.log("");
          process.exit();
        }
        await PM.publish(db_host, db_port);
        break;
      case 'install':
        await PM.install(db_host, db_port);
        break;
      default:
        console.log("command =", command);
        console.error('Invalid command!');
    }

  } catch (e) {
    console.error(e.stack);
  }
})();

/*
request.post({
  url:db_addr+'users-auth.io',
  form: {
    data: JSON.stringify({
      email: 'qualphey@gmail.com',
      pwd: '',
      pwdr: '',
      command: "register"
    })
  }
}, function(err, res, body) {
  if (err) console.error(err)
//  console.log(res);
  console.log(body);
});
*/
/*
const { parse } = require('url')
const { basename } = require('path')

const TIMEOUT = 10000

let download = async function(url, path) {
  try {
    console.log(url);
    const uri = parse(url)
    if (!path) {
      path = basename(uri.path)
    }
    const file = fs.createWriteStream(path)

    return new Promise(function(resolve, reject) {
      const request = http.get(uri.href).on('response', function(res) {
        const len = parseInt(res.headers['content-length'], 10)
        let downloaded = 0
        let percent = 0
        res
          .on('data', function(chunk) {
            file.write(chunk)
            downloaded += chunk.length
            percent = (100.0 * downloaded / len).toFixed(2)
            process.stdout.write(`Downloading ${percent}% ${downloaded} bytes\r`)
          })
          .on('end', function() {
            file.end()
            console.log(`${uri.path} downloaded to: ${path}`)
            resolve()
          })
          .on('error', function (err) {
            reject(err)
          })
      })
      request.setTimeout(TIMEOUT, function() {
        request.abort()
        reject(new Error(`request timeout after ${TIMEOUT / 1000.0}s`))
      })
    })
  } catch (e) {
    console.error(e.stack);
  }
};
(async function() {
  try {

    request.postSync = async function(url, data, access_token) {
      return await new Promise(function(resolve) {
        request.post({
          url: url,
          form: {
            data: JSON.stringify(data),
            access_token: access_token
          }
        }, function(err, res, body) {
          if (err) console.error(err)
          console.log("BODY", body);
          resolve(JSON.parse(body));
        });
      }); 
    };

    request.getSync = async function(url, data, access_token) {
      return await new Promise(function(resolve) {
        request.get(url+"?data="+JSON.stringify(data), function(err, res, body) {
          if (err) console.error(err)
          resolve(JSON.parse(body));
        });
      }); 
    };



    let mutableStdout = new Writable({
      write: function(chunk, encoding, callback) {
        if (!this.muted)
          process.stdout.write(chunk, encoding);
        callback();
      }
    });

    let rl = readline.createInterface({
      input: process.stdin,
      output: mutableStdout,
      terminal: true
    });

    rl.questionSync = async function(msg, pwd) {
      return await new Promise(function(resolve) {
        mutableStdout.muted = false;
        rl.question(msg, function(input) {
          resolve(input);
        });
        if (pwd) mutableStdout.muted = true;
      });
    };

    let pub_key = await request.getSync(db_addr+'users-auth.io', {
      command: "get_public_key"
    });

    switch(command) {
      case 'init':
        let init_path = process.cwd();
        let init_pages_path = path.resolve(init_path, "pages");
        if (fs.existsSync(init_path)) {
          console.log("COPY", path.resolve(__dirname, "template"), init_path);
          fs.copySync(path.resolve(__dirname, "template"), init_path, { overwrite: false });
        } else {
          throw new Error("CWD not found! Directory `"+init_path+"` does not exist.");
        }
        break;
      case 'publish':

        let config_path = path.resolve(process.cwd(), 'module.json');
        let config = false;
        if (fs.existsSync(config_path)) {
          config = JSON.parse(fs.readFileSync(config_path, 'utf8'));
          if (!config.type) throw new Error("No module type!");
          if (!config.name) throw new Error("No module name!");
          if (!config.description) throw new Error("No module decription!");
          if (!config.version) throw new Error("No module version!"); 
          if (!config.repository) throw new Error("No module repository!");

          console.log('config', config);
        } else {
          throw new Error("No module found! File `"+config_path+"` does not exist.");
        }

        console.log("Authentication:");




        const email = await rl.questionSync('Your email on mellisuga.org: ');
        const password = await rl.questionSync('Password: ', true);

        rl.close();
        console.log("-*-*-*- Authenticating...");
        let body = await request.postSync(db_addr+'users-auth.io', {
          email: email,
          pwd: password,
          command: "authenticate"
        });

        let result = await request.postSync(db_addr+'modules.io', {
          command: "publish", module: config
        }, body.access_token);
        console.log(result);
        break;
      case 'install':
        let cms_cfg = path.resolve(process.cwd(), "mellisuga.json");
        if (fs.existsSync(cms_cfg)) {
          const type = process.argv[3];
          const name = process.argv[4];
          if (type === "cms" || type === "web") {
            if (name) {
              let result = await request.getSync(db_addr+'modules.io', {
                command: "install", type: type, name: name
              });
              if (result.success) {
                const nfilename = type+"-"+name+"@"+result.success.version+".tar.gz";

                if (!fs.existsSync(cache_dir)) fs.mkdirSync(cache_dir);
                const nfilepath = path.resolve(cache_dir, nfilename);
                console.log(db_addr+"modb/"+type+"/"+name+"/"+name+"@"+result.success.version+".tar.gz", nfilepath);
                await download(db_addr+"modb/"+type+"/"+name+"/"+name+"@"+result.success.version+".tar.gz", nfilepath);

                let modules_dir = path.resolve(process.cwd(), "mellisuga_modules");
                const cachmod_path = path.resolve(cache_dir, type+"-"+name+"@"+result.success.version);
                const module_path = path.resolve(modules_dir, type, name);
                console.log(nfilepath, cachmod_path);
                await compressing.tgz.uncompress(nfilepath, module_path).then(function(err) {
                  console.log(err);

                  console.log("DONE");
                })
                .catch(function(err) {
                  console.log(err.stack);
                });
                console.log("Installing:", type, name);
          *//*      let modules_dir = path.resolve(process.cwd(), "mellisuga_modules");
                const module_path = path.resolve(modules_dir, type, name);process.cwd(
                if (fs.existsSync(module_path)) {
                  const existing_version = JSON.parse(fs.readFileSync(
                  path.resolve(modules_dir, type, name, 'module.json'), 'utf8')).version;
                  if (versionCompare(result.success.version, existing_version) > 0) {
                    console.log("Updating: ", type, name, existing_version, '->', result.success.version);
                    const simpleGit = require('simple-git')(path.resolve(modules_dir, name));
                    simpleGit.pull().exec(() => {
                      console.log('finished')
                      process.exit();
                    })
                  } else {
                     console.log("Module is already at its latest version!");
                  }
                } else {
                  const simpleGit = require('simple-git')(path.resolve(modules_dir, type));
                  simpleGit.clone(result.success.repo, module_path).exec(() => process.exit());
                }*//*
              }
            } else {
              console.error("No module name!");
            }
          } else {
            console.error("Invalid module type!");
          }
        } else {
          console.error("No `mellisuga.json` file in cwd!");
          console.log("Run this command in the root directory of a mellisuga project.");
        }
        break;
      case "search":
        const value = process.argv[3];
        if (value) {
          let result = await request.getSync(db_addr+'modules.io', {
            command: "search", search_val: value
          });
          for (let r = 0; r < result.length; r++) {
            console.log(result[r].type, '~', result[r].name+"@"+result[r].version);
          }
        } else {
          console.error("No search keywords specified!");
        }
        break;
      default:
        console.error('No arguments specified!');
    }
    process.exit();
  } catch (e) {
    console.error(e.stack);
  }
})();

function versionCompare(v1, v2, options) {
    var lexicographical = options && options.lexicographical,
        zeroExtend = options && options.zeroExtend,
        v1parts = v1.split('.'),
        v2parts = v2.split('.');

    function isValidPart(x) {
        return (lexicographical ? /^\d+[A-Za-z]*$/ : /^\d+$/).test(x);
    }

    if (!v1parts.every(isValidPart) || !v2parts.every(isValidPart)) {
        return NaN;
    }

    if (zeroExtend) {
        while (v1parts.length < v2parts.length) v1parts.push("0");
        while (v2parts.length < v1parts.length) v2parts.push("0");
    }

    if (!lexicographical) {
        v1parts = v1parts.map(Number);
        v2parts = v2parts.map(Number);
    }

    for (var i = 0; i < v1parts.length; ++i) {
        if (v2parts.length == i) {
            return 1;
        }

        if (v1parts[i] == v2parts[i]) {
            continue;
        }
        else if (v1parts[i] > v2parts[i]) {
            return 1;
        }
        else {
            return -1;
        }
    }

    if (v1parts.length != v2parts.length) {
        return -1;
    }

    return 0;
}

*/

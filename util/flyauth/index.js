
import os from 'os'

import fs from 'fs'
import path from 'path'

import crypto from 'crypto'
import pako from 'pako'

import XHR from "../xhr_async.js"
import { AES, RSA } from "./crypt.js"

const data_dir_path = path.resolve(os.homedir(), ".mellisuga");
if (!fs.existsSync(data_dir_path)) fs.mkdirSync(data_dir_path);
const cache_path = path.resolve(data_dir_path, "cache.json");

import readline from 'readline'
import { Writable } from 'stream'

export default class Flyauth {
  static async construct(db_host, db_port) {
    try {
      let pub_key = undefined, cfg = undefined;
      try {
        pub_key = await XHR.get(db_host, db_port, "/users-auth.io", {
          command: "get_public_key"
        });
        cfg = await XHR.get(db_host, db_port, "/users-auth.io", {
          command: "get_configuration"
        });
      } catch (e) {
        console.error(e.stack);
      }
      console.log("CONTINUED");
      let _this = new Flyauth(db_host, db_port, pub_key, cfg);
      
      return _this;
    } catch (e) {
      console.error(e.stack);
    }
  }

  async set_authentials(sess) {
    try {
      let credentials = {
        fp: sess.fingerprint,
        csrf: sess.csrf_token
      };
      let encreds = await RSA.encrypt(JSON.stringify(credentials), this.rsa_key);
      global.authentials = encodeURIComponent(Buffer.from(pako.deflate(Uint8Array.from(Buffer.from(encreds, 'hex')))).toString("base64"));
      global.xss_token = sess.xss_token
    } catch (e) {
      console.error(e.stack);
    }
  }

  get_session() {
    let cur_session = undefined;
    if (fs.existsSync(cache_path)) {
      cur_session = JSON.parse(fs.readFileSync(cache_path, 'utf8'));
    }
    return cur_session
  }

  async user_session(signin) {
    let cur_session = this.get_session();

    if (!cur_session) {
      cur_session = await this.authenticate();
      if (cur_session) {
        fs.writeFileSync(cache_path, JSON.stringify(cur_session));
        await this.set_authentials(cur_session);
      }
    } else {
      if (signin) {
        console.log("\x1b[33m>>");
        console.log(">> You are currently signed in as:", cur_session.user);
        console.log(">> This account will be automaticly signed out if you continue.");
        console.log(">>\x1b[0m");
        cur_session = await this.authenticate();
        if (cur_session) {
          await this.set_authentials(cur_session);
          fs.writeFileSync(cache_path, JSON.stringify(cur_session));
        }
      } else {
        if (cur_session.token_expiration-Date.now() <= 0) {
          console.log("");
          console.log("\x1b[33mSession expired for:", cur_session.user, "\x1b[0m");
          cur_session = await this.authenticate(cur_session.user);
          if (cur_session) fs.writeFileSync(cache_path, JSON.stringify(cur_session));
        } else {
          await this.set_authentials(cur_session);
          if (cur_session.token_expiration-Date.now() <= this.cfg.token_expiration_delay*1000) {
            let renewed = await this.renew_session(cur_session.user);
            cur_session.csrf_token = renewed.csrf_token;
            cur_session.token_expiration = renewed.token_expiration;
            fs.writeFileSync(cache_path, JSON.stringify(cur_session));
          } else {
            console.log("User:", cur_session.user);
          }
        }
      }

    }
    return cur_session;
  }

  async authenticate(user) {
    try {
      console.log("");
      console.log("\x1b[36m>>> Authenticate your account on mellisuga.org <<<\x1b[0m");

      let mutableStdout = new Writable({
        write: function(chunk, encoding, callback) {
          if (!this.muted)
            process.stdout.write(chunk, encoding);
          callback();
        }
      });

      let rl = readline.createInterface({
        input: process.stdin,
        output: mutableStdout,
        terminal: true
      });

      rl.questionSync = async function(msg, pwd) {
        return await new Promise(function(resolve) {
          mutableStdout.muted = false;
          rl.question(msg, function(input) {
            resolve(input);
          });
          if (pwd) mutableStdout.muted = true;
        });
      };

      const email = user || await rl.questionSync('Email: ');
      const password = await rl.questionSync('Password: ', true);
      rl.close();

      const fprint = JSON.stringify([
        "mellisuga-cli",
        os.platform(),
        crypto.randomBytes(32).toString("base64"),
        crypto.randomBytes(32).toString("base64"),
        crypto.randomBytes(32).toString("base64")
      ])

      console.log("-*-*-*- Authenticating...");
      let aes = new AES();
      let auth_data = {
        command: "authenticate",
        email: email,
        pwd: password,
        fp: fprint,
        aes_key: aes.key
      };
  //    if (hash) auth_data.security_hash = hash;


      let encrypted_auth_data = await RSA.encrypt(JSON.stringify(auth_data), this.rsa_key);
      let resp = await XHR.post(this.host, this.port, "/users-auth.io", encrypted_auth_data);
      if (typeof resp === 'object') {
        let texp_str = Date.now()+(this.cfg.token_expiration_time+this.cfg.token_expiration_delay)*1000;
        
        return {
          user: email,
          csrf_token: aes.decrypt(resp.access_token),
          xss_token: global.xss_token,
          token_expiration: texp_str,
          fingerprint: fprint
        };
      } else {
        return undefined;
      }
    } catch (e) {
      console.error(e.stack);
    }
  }

  async renew_session(user) {
    try {
      console.log("\x1b[32mRenewing session for:", user, "\x1b[0m");

      let renew_path = "/users-auth.io-restricted";

      let aes = new AES();
      let enc_data = await RSA.encrypt({
        command: "renew-token",
        aes_key: aes.key
      }, this.rsa_key);
      let new_token = await XHR.post(this.host, this.port, renew_path, enc_data);

      if (typeof new_token === "string") {
        console.log("\x1b[31mFAILED!\x1b[0m");
        await this.authenticate(user);
      } 

      console.log("NEW TOKEN", new_token);

      let texp_str = Date.now()+(this.cfg.token_expiration_time+this.cfg.token_expiration_delay)*1000;
      return {
        csrf_token: aes.decrypt(new_token.data),
        token_expiration: texp_str
      };
    } catch (e) {
      console.error(e.stack);
    }
  }

  async terminate_session() {
    try {
      let cur_session = this.get_session();
      if (!cur_session) {
        console.log("\x1b[33mYou are not signed in!\x1b[0m");
      } else {
        console.log("\x1b[36mSigning out:", cur_session.user, "\x1b[0m");
        let result = await XHR.post(this.host, this.port, "/users-auth.io", {
          command: "terminate"
        });
        fs.unlinkSync(cache_path, 'utf8');
      }
    } catch (e) {
      console.error(e.stack);
    }
  }

  constructor(db_host, db_port, rsa_key, cfg) {
    this.host = db_host;
    this.port = db_port;
    this.rsa_key = rsa_key;
    this.cfg = cfg;

    console.log("RSA", rsa_key);
  }
}

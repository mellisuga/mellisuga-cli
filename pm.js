
import child_process from "child_process"

import os from 'os'

import compressing from 'compressing'

import fs from 'fs-extra'
import path from 'path'

import XHR from "./util/xhr_async.js"

const data_dir_path = path.resolve(os.homedir(), ".mellisuga");

const module_types = [
  "waf", "web", "theme", "app-template", "page-template", "cli-script", "group"
]


export default class PM {
  static async create_directories() {
    let config_path = path.resolve(process.cwd(), 'mellisuga.json');
    if (fs.existsSync(config_path)) {
      const modules_path = path.resolve(process.cwd(), 'mellisuga_modules');
      if (!fs.existsSync(modules_path)) fs.mkdirSync(modules_path);

      //WAF
      const waf_path = path.resolve(modules_path, 'waf');
      if (!fs.existsSync(waf_path)) fs.mkdirSync(waf_path);

      const waf_core_path = path.resolve(waf_path, 'core');
      if (!fs.existsSync(waf_core_path)) fs.mkdirSync(waf_core_path);

      const waf_community_path = path.resolve(waf_path, 'community');
      if (!fs.existsSync(waf_community_path)) fs.mkdirSync(waf_community_path);

      //WEB
      const web_path = path.resolve(modules_path, 'web');
      if (!fs.existsSync(web_path)) fs.mkdirSync(web_path);

      const web_core_path = path.resolve(web_path, 'core');
      if (!fs.existsSync(web_core_path)) fs.mkdirSync(web_core_path);

      const web_community_path = path.resolve(web_path, 'community');
      if (!fs.existsSync(web_community_path)) fs.mkdirSync(web_community_path);
    }
  }

  static async init() {
    try {
      let name = process.argv[4]; 
      let template = process.argv[5];
      if (!name) name = "new_mellisuga_app";
      if (!template) template = "quick";

      let new_project_path = path.resolve(process.cwd(), name);
      if (fs.existsSync(new_project_path)) {
        console.log("Directory `"+new_project_path+"` already exists!");
        process.exit();
      }

      let template_path = path.resolve(data_dir_path, "templates", "app", template);

      if (!fs.existsSync(template_path)) {
        console.log("Error loading template `"+name+"`!");
        console.log("Directory `"+template_path+"` does not exist!");
        process.exit();
      }

      if (fs.lstatSync(template_path).isSymbolicLink()) {
        template_path = fs.readlinkSync(template_path);
      }

      console.log("Copying `"+template_path+"` to `"+new_project_path+"`");
      await new Promise(function(out) {
        fs.copy(template_path, new_project_path, function (err) {
          if (err) return console.error(err)
          out();
        });
      });

    } catch (e) {
      console.error(e.stack);
    }
  }

  static async new_page() {
    try {
      if (!fs.existsSync(path.resolve(process.cwd(), "mellisuga.json"))) {
        console.log("Error: this is not a mellisuga app root!");
        process.exit();
      }

      let name = process.argv[4]; 
      let template = process.argv[5];
      if (!name) name = "new_page";
      if (!template) template = "quick";

      let new_project_path = path.resolve(process.cwd(), "pages", name);
      if (fs.existsSync(new_project_path)) {
        console.log("Directory `"+new_project_path+"` already exists!");
        process.exit();
      }

      let template_path = path.resolve(data_dir_path, "templates", "page", template);

      if (!fs.existsSync(template_path)) {
        console.log("Error loading template `"+name+"`!");
        console.log("Directory `"+template_path+"` does not exist!");
        process.exit();
      }

      if (fs.lstatSync(template_path).isSymbolicLink()) {
        template_path = fs.readlinkSync(template_path);
      }

      console.log("Copying `"+template_path+"` to `"+new_project_path+"`");
      await new Promise(function(out) {
        fs.copy(template_path, new_project_path, function (err) {
          if (err) return console.error(err)
          out();
        });
      });
    } catch (e) {
      console.error(e.stack);
    }
  }

  static async publish(db_host, db_port) {
    try {
      let _this = new PM();

      let config_path = path.resolve(process.cwd(), 'module.json');
      let config = false;
      if (fs.existsSync(config_path)) {
        config = JSON.parse(fs.readFileSync(config_path, 'utf8'));
        if (!config.type) throw new Error("No module type!");
        if (!module_types.includes(config.type)) throw new Error("Invalid module type!");
        if (!config.name) throw new Error("No module name!");
        if (!config.description) throw new Error("No module decription!");
        if (!config.version) throw new Error("No module version!"); 
        if (!config.repository) throw new Error("No module repository!");

        console.log('config', config);
      } else {
        throw new Error("No module found! File `"+config_path+"` does not exist.");
      }

      let pubres = await XHR.post(db_host, db_port, "/modules.io", {
        command: "publish", module: config
      });

      console.log(pubres);
      
      return _this;
    } catch (e) {
      console.error(e.stack);
    }
  }

  static async install(db_host, db_port) {
    try {
      const type = process.argv[3];
      const name = process.argv[4];
      if (!name) {
        let config_path = path.resolve(process.cwd(), 'mellisuga.json');
        if (fs.existsSync(config_path)) {
          async function install_from_cfg(cpath, what, corepkg) {
            try {
              let trust_level = corepkg ? "core" : "community";

              let cstr = fs.readFileSync(cpath, "utf8");
              console.log("INSTALL", what, cpath);
              let mcfg = JSON.parse(cstr);
              //console.log("INSTALL", cstr);

              let install_path = undefined;
              

              console.log("PACKAGE >>>", what, trust_level, cpath);

              if (mcfg.dependencies && typeof mcfg.dependencies[trust_level] === "object" && mcfg.dependencies[trust_level] !== null
                && typeof mcfg.dependencies[trust_level][what] === "object" && mcfg.dependencies[trust_level][what] !== null) {
                let cli_config_path = path.resolve(data_dir_path, "config.json");
                let cli_config = {};
                if (fs.existsSync(cli_config_path)) {
                  try {
                    cli_config = JSON.parse(fs.readFileSync(cli_config_path, "utf8"));
                  } catch (e) {
                    console.error(e.stack);
                    process.exit();
                  }
                }



                for (let d in mcfg.dependencies[trust_level][what]) {
                  let dmodule = mcfg.dependencies[trust_level][what][d];
                  const modules_path = path.resolve(process.cwd(), "mellisuga_modules") //, what, d);     

                  if (dmodule.match(/^(\d+\.)?(\d+\.)?(\*|\d+)$/g)) {

                    install_path = path.resolve(modules_path, what, corepkg ? "core" : "community", d);
                    
                    let local_path = undefined;
                    if (cli_config.local_packages && cli_config.local_packages[what]) {
                      fs.readdirSync(cli_config.local_packages[what]).forEach(function(lpkgname) {
                        if (lpkgname === d) {
                          local_path =  path.resolve(cli_config.local_packages[what], lpkgname);
                        }
                      });
                    }


                    if (!local_path) {
                 //     console.log("");
                      console.log("\x1b[36mInstalling from database \x1b[32m"+d+"\x1b[0m");

                      if (fs.existsSync(install_path)) {
                        if (fs.lstatSync(install_path).isDirectory()) {
                          fs.rmSync(install_path, { recursive: true, force: true });
                        } else {
                          fs.rmSync(install_path, { force: true });
                        }
                      }

                      const download_path = path.resolve(os.homedir(), ".mellisuga", d+"@"+dmodule+".tar.gz");
                      console.log(" <<< DOWNLOADING <<< http://"+db_host+":"+db_port+"/modb/"+what+"/"+d+"/"+d+"@"+dmodule+".tar.gz");
                      await XHR.get_file("https://"+db_host+":"+db_port+"/modb/"+what+"/"+d+"/"+d+"@"+dmodule+".tar.gz", download_path);


                      await new Promise(async (out) => {
                        await compressing.tgz.uncompress(download_path, install_path).then(function(err) {
                          console.log(err);
                          console.log("Cleaning up", ">>", "rm", download_path);
                          fs.rmSync(download_path, { force: true });
                          console.log("DONE");
                          out();
                        }).catch(function(err) {
                          console.log(err.stack);
                          console.log("Cleaning up", ">>", "rm", download_path);
                          fs.rmSync(download_path, { force: true });
                          out();
                        });

                      });
                    } else {
                      dmodule = local_path;
                 //     console.log("");
                      
                      console.log("\x1b[36mInstalling from disk \x1b[32m"+d+"\x1b[0m", ">>>", what+"/"+trust_level);
                      console.log("Symlink "+dmodule);

                      install_path = path.resolve(modules_path, what, trust_level, d);
                      if (fs.existsSync(install_path)) {
                        if (fs.lstatSync(install_path).isDirectory()) {
                          console.log("rmdir");
                          fs.rmSync(install_path, { recursive: true, force: true });
                        } else if (fs.lstatSync(install_path).isSymbolicLink()) {
                          fs.unlinkSync(install_path);
                        } 
                      }
                      fs.symlinkSync(dmodule, install_path, "dir");
                      install_path = dmodule;
                    }



                    if (!local_path && fs.existsSync(path.resolve(install_path, "package.json"))) {
                      child_process.execSync("cd "+install_path+" && npm install", {stdio: 'inherit'});
                    } else if (local_path && fs.existsSync(path.resolve(install_path, "ldeps.sh"))) {
                      child_process.execSync("cd "+install_path+" && sh ldeps.sh", {stdio: 'inherit'});
                    } else if (local_path && fs.existsSync(path.resolve(install_path, "local_deps.sh"))) {
                      child_process.execSync("cd "+install_path+" && sh local_deps.sh", {stdio: 'inherit'});
                    }

                    let subjson_path = path.resolve(install_path, "module.json");
                    if (fs.existsSync(subjson_path)) {
                      await install_from_cfg(subjson_path, "waf", true);
                      await install_from_cfg(subjson_path, "web", true);
                    }
                  } else {
                    console.log("\x1b[31mInvalid dependency `"+d+"` pointing to `"+dmodule+"` in `"+config_path+"`\x1b[0m");
                    process.exit();
                  }
                }
              }
              if (corepkg) await install_from_cfg(cpath, what);
            } catch (e) {
              console.error(e.stack);
              process.exit();
            }
          }

          await install_from_cfg(config_path, "waf", true);
          await install_from_cfg(config_path, "web", true);
        }
      } else  {

      }
    } catch (e) {
      console.error(e.stack);
    }
  }
}
